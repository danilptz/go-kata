package main

import (
	"fmt"
)

func main() {
	typeInt()
}

func typeInt() {
	fmt.Println("=== START type int ===")
	var numberUint8 uint8 = 1 << 7
	var fromInt8 = int8(numberUint8)
	numberUint8--
	var toInt8 = int8(numberUint8)
	fmt.Println("fromInt8:", fromInt8, "toInt8:", toInt8)
	var numberUint16 uint16 = 1 << 15
	var fromInt16 = int16(numberUint16)
	numberUint16--
	var toInt16 = int16(numberUint16)
	fmt.Println("fromInt16:", fromInt16, "toInt16:", toInt16)
	var numberUint32 uint32 = 1 << 31
	var fromInt32 = int32(numberUint32)
	numberUint32--
	var toInt32 = int32(numberUint32)
	fmt.Println("fromInt32:", fromInt32, "toInt32:", toInt32)
	var numberUint64 uint64 = 1 << 63
	var fromInt64 = int64(numberUint64)
	numberUint64--
	var toInt64 = int64(numberUint64)
	fmt.Println("fromInt64:", fromInt64, "toInt64:", toInt64)
	fmt.Println("=== END type int ===")
}
