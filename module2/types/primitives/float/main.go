package main

import (
	"fmt"
	"unsafe"
)

func main() {
	typeFloat()
}

func typeFloat() {
	fmt.Println("=== START type float ===")
	var uintNumber uint32 = 1 << 29
	uintNumber += 1 << 28
	uintNumber += 1 << 27
	uintNumber += 1 << 26
	uintNumber += 1 << 25
	uintNumber += 1 << 21
	uintNumber += 1 << 31

	floatNumber := *(*float32)(unsafe.Pointer(&uintNumber))
	fmt.Println(floatNumber)

	a, b := 2.3329, 3.1234
	c := a + b
	fmt.Println("пример ошибки1", c)

	a = 9.99999
	b2 := a
	fmt.Println("пример ошибки 2", b2)

	a = 999998455
	b3 := float32(a)
	fmt.Printf("пример ошибки 3: %f\n", b3)

	a4 := 5.2
	b4 := 4.1
	fmt.Println(a4, b4)
	fmt.Println(true)

	c4 := 5.2
	d4 := 2.1

	fmt.Println(c4 + d4)
	fmt.Println(false)

	fmt.Println("=== END type float ===")

}
