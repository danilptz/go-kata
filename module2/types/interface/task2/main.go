// You can edit this code!
// Click here and start typing.
package main

import (
	"fmt"
)

type User struct {
	ID   int
	Name string
}

func (u *User) GetName() string {
	return u.Name
}

type Userer interface {
	GetName() string
}

func main() {

	u := &User{
		ID:   34,
		Name: "Annet",
	}
	_ = Userer(u)
	user := *u
	testUserName(user)
}

func testUserName(u User) {
	if u.GetName() == "Annet" {
		fmt.Println("Success!")
	}
}
