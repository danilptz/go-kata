// You can edit this code!
// Click here and start typing.
package main

import "fmt"

type User struct {
	ID   int
	Name string
}

func (u *User) GetName() string {
	return u.Name
}

type Userer interface {
	GetName() string
}

func main() {
	user := &User{
		ID:   123,
		Name: "Alex",
	}
	i := Userer(user)
	_ = i
	fmt.Println("Success!")
}
