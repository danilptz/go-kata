package main

import "fmt"

type Project struct {
	Name  string
	Stars int
}

func main() {
	projects := []Project{
		{
			Name:  "https://github.com/docker/compose",
			Stars: 27600,
		},
		{
			Name:  "https://github.com/dreamworksanimation/openmoonray",
			Stars: 500,
		},
		{
			Name:  "https://github.com/comfyanonymous/ComfyUI",
			Stars: 477,
		},
		{
			Name:  "https://github.com/logspace-ai/langflow",
			Stars: 113,
		},
		{
			Name:  "https://github.com/setzer22/llama-rs",
			Stars: 353,
		},
		{
			Name:  "https://github.com/ayaka14732/ChatGPTAPIFree",
			Stars: 107,
		},
		{
			Name:  "https://github.com/whoiskatrin/sql-translator",
			Stars: 321,
		},
		{
			Name:  "https://github.com/acheong08/EdgeGPT",
			Stars: 243,
		},
		{
			Name:  "https://github.com/apache/incubator-opendal",
			Stars: 51,
		},
		{
			Name:  "https://github.com/THUDM/ChatGLM-6B",
			Stars: 1259,
		},
		{
			Name:  "https://github.com/tloen/alpaca-lora",
			Stars: 348,
		},
		{
			Name:  "https://github.com/xx025/carrot",
			Stars: 883,
		},
		{
			Name:  "https://github.com/brunosimon/folio-2019",
			Stars: 130,
		},
	}
	punct := "-"
	for i := 0; i < 60; i++ {
		punct += "-"
	}

	m := make(map[string]Project)
	for i, v := range projects {
		m[SliceMapKey(v.Name)] = projects[i]
	}

	for k, v := range m {
		fmt.Printf("key: %v\nvalue: %v\n%s\n", k, v, punct)
	}
	// в цикле запишите в map
	// в цикле пройдитесь по мапе и выведите значения в консоль

}

func SliceMapKey(url string) string {
	var lst []string
	index := 0
	count := 0

	for i, v := range url {
		if string(v) == "/" {
			count++
			if count == 4 {
				index = i
				break
			}
		}
	}

	lst = append(lst, url[index+1:])
	mapKey := lst[0]

	return mapKey
}
