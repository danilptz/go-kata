package main

import (
	"fmt"
	"unsafe"
)

type User struct {
	Age      int
	Name     string
	Wallet   Wallet
	Location Location
}

type Wallet struct {
	RUR uint64
	USD uint64
	BTC uint64
	ETH uint64
}

type Location struct {
	Adress string
	City   string
	index  string
}

func main() {
	user := User{
		Age:  13,
		Name: "Alexander",
	}
	fmt.Println(user)
	wallet := Wallet{
		RUR: 250000,
		USD: 35000,
		BTC: 1,
		ETH: 4,
	}
	fmt.Println(wallet)
	fmt.Println("wallet allocates:", unsafe.Sizeof(wallet), "bytes")
	user.Wallet = wallet
	fmt.Println(user)

	user2 := User{
		Age:  34,
		Name: "Anton",
		Wallet: Wallet{
			RUR: 144000,
			USD: 8900,
			BTC: 55,
			ETH: 34,
		},
		Location: Location{
			Adress: "Нововатутинская 3-я ул, 13, к.2",
			City:   "Москва",
			index:  "108836",
		},
	}
	fmt.Println(user2)
}
