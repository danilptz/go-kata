package main

import "fmt"

func main() {
	s := []int{1, 2, 3}
	sAppend := Append(s)
	fmt.Println("append:", sAppend)
	sExtend := Extend(s)
	fmt.Println("extend:", sExtend)
}

func Append(s []int) []int {
	s = append(s, 4)
	return s
}

func Extend(s []int) []int {
	n := len(s)
	slice := make([]int, n, 2*n+1)
	copy(slice, s)
	s = slice
	s = s[0 : n+1]
	s[n] = 4
	return s
}
