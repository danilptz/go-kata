package main

import "fmt"

type Calc struct {
	a, b   float64
	result float64
}

func NewCalc() *Calc {
	// конструктор калькулятора
	return &Calc{}
}

type Funcs func(a, b float64) float64

func (c *Calc) SetA(a float64) {
	c.a = a
}

func (c *Calc) SetB(b float64) {
	c.b = b
}

func (c *Calc) Do(operation func(a, b float64) float64) {
	c.result = operation(c.a, c.b)
}

func (c *Calc) Result() float64 {
	return c.result
}

func Multiply(a, b float64) float64 { // реализуйте по примеру divide, sum, average
	return a * b
}

func Divide(a, b float64) float64 { // реализуйте по примеру divide, sum, average
	return a / b
}

func Sum(a, b float64) float64 { // реализуйте по примеру divide, sum, average
	return a + b
}

func Average(a, b float64) float64 { // реализуйте по примеру divide, sum, average
	return (a + b) / 2
}

func main() {
	funcs := []Funcs{Multiply, Divide, Sum, Average}
	a := float64(10)
	b := float64(34)
	calc := NewCalc()
	calc.SetA(a)
	calc.SetB(b)
	res2 := calc.Result()
	for _, v := range funcs {
		calc.Do(v)
		res := calc.Result()
		fmt.Println(res)
		if res == res2 {
			panic("object statement is not persist")
		}
		res2 = calc.Result()
		fmt.Println(res2)
	}
}
