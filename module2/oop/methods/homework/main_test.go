package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestMultiply(t *testing.T) {
	testCases := []Calc{}
	for i := 0; i < 9; i++ {
		if i != 0 {
			test := Calc{
				a:      float64(i + 5),
				b:      float64(i),
				result: float64(i+5) * float64(i),
			}
			testCases = append(testCases, test)
		}
	}
	for _, test := range testCases {
		t.Run("multiply", func(t *testing.T) {
			assert.Equal(t, test.result, Multiply(test.a, test.b))
		})

	}

}

func TestDivide(t *testing.T) {
	testCases := []Calc{}
	for i := 0; i < 9; i++ {
		if i != 0 {
			test := Calc{
				a:      float64(i + 5),
				b:      float64(i),
				result: float64(i+5) / float64(i),
			}
			testCases = append(testCases, test)
		}
	}
	for _, test := range testCases {
		t.Run("divide", func(t *testing.T) {
			assert.Equal(t, test.result, Divide(test.a, test.b))
		})

	}
}

func TestSum(t *testing.T) {
	testCases := []Calc{}
	for i := 0; i < 9; i++ {
		if i != 0 {
			test := Calc{
				a:      float64(i + 5),
				b:      float64(i),
				result: float64(i+5) + float64(i),
			}
			testCases = append(testCases, test)
		}
	}
	for _, test := range testCases {
		t.Run("sum", func(t *testing.T) {
			assert.Equal(t, test.result, Sum(test.a, test.b))
		})

	}
}

func TestAverage(t *testing.T) {
	testCases := []Calc{}
	for i := 0; i < 9; i++ {
		if i != 0 {
			test := Calc{
				a:      float64(i + 5),
				b:      float64(i),
				result: (float64(i+5) + float64(i)) / 2,
			}
			testCases = append(testCases, test)
		}
	}
	for _, test := range testCases {
		t.Run("average", func(t *testing.T) {
			assert.Equal(t, test.result, Average(test.a, test.b))
		})

	}
}
