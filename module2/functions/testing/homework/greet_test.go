package homework

import (
	"fmt"
	"testing"
	"unicode"
)

func TestGreet(t *testing.T) {
	type args struct {
		name string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "english name",
			args: args{name: "John"},
			want: "Hello John, you welcome!",
		},
		{
			name: "russian name",
			args: args{name: "Боря"},
			want: "Привет Боря, добро пожаловать!",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Greet(tt.args.name); got != tt.want {
				t.Errorf("Greet() = %v, want: %v", got, tt.want)
			}
		})
	}
}

func Greet(name string) string {
	n := []rune(name)
	c := 0
	for _, v := range n {
		if unicode.Is(unicode.Latin, v) == false {
			c++
		}
	}
	if c == 0 {
		return fmt.Sprintf("Hello %s, you welcome!", name)
	} else {

		c = 0
		for _, v := range n {
			if unicode.Is(unicode.Cyrillic, v) {
				c++
			}
		}
		if c == len(n) {
			return fmt.Sprintf("Привет %s, добро пожаловать!", name)
		} else {
			return "Incorrect name!"
		}
	}
}

// Спасибо большое за направление, такую нужную штуку открыл для себя, код гараздо читабельнее стал)) поспешил и доконца в unicode не вник, а оказывается
// столько еще может полезного. Жаль конечно, что торопиться приходится. Еще раз, спасибо))
