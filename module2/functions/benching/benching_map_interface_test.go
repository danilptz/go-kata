package benching

import "testing"

func InsertXInterfaceMap(x int, b *testing.B) {
	testmap := make(map[interface{}]int, 0)
	b.ResetTimer()
	for i := 0; i < x; i++ {
		testmap[i] = i
	}
}
func BenchmarkInsertInterfaceMap100000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		InsertXInterfaceMap(100000, b)
	}
}

func BenchmarkInsertInterfaceMap10000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		InsertXInterfaceMap(10000, b)
	}
}

func BenchmarkInsertInterfaceMap1000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		InsertXInterfaceMap(1000, b)
	}
}
