package benching

import "testing"

func insertXPreallocIntMap(x int, b *testing.B) {
	testMap := make(map[int]int, x)
	b.ResetTimer()
	for i := 0; i < x; i++ {
		testMap[i] = i
	}
}

func BenchmarkInsertIntMapPrealloc100000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		insertXPreallocIntMap(100000, b)
	}
}

func BenchmarkInsertIntMapPrealloc10000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		insertXPreallocIntMap(10000, b)
	}
}

func BenchmarkInsertIntMapPrealloc1000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		insertXPreallocIntMap(1000, b)
	}
}
