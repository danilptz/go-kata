package benching

import (
	"math/rand"
	"testing"
)

// Создаем срезы и карты для бенчмарка, чтобы не заниматься этим при каждой итерации
var userSlice10000 []User
var userSlice100000 []User
var userSlice1000000 []User

var userMap10000 map[int]User
var userMap100000 map[int]User
var userMap1000000 map[int]User

// Имена Применяем списки неслучайных имен для генерации случайных пользователей (Random Users)
var Names []string = []string{"Jasper", "Johan", "Edward", "Niel", "Percy", "Adam", "Grape", "Sam", "Redis", "Jennifer", "Jessica", "Angelica", "Amber", "Watch"}

// Фамилии Применяем списки неслучайных имен для генерации случайных пользователей
var SirNames []string = []string{"Ericsson", "Redisson", "Edisson", "Tesla", "Bolmer", "Andersson", "Sword", "Fish", "Coder"}

var IDCounter int

type User struct {
	ID   int
	Name string
}

func init() {
	userMap10000 = generateMapUsers(10000)
	userMap100000 = generateMapUsers(100000)
	userMap1000000 = generateMapUsers(1000000)

	userSlice10000 = generateSliceUsers(10000)
	userSlice100000 = generateSliceUsers(100000)
	userSlice1000000 = generateSliceUsers(1000000)

	// Перемешиваем срез
	rand.Shuffle(len(userSlice10000), func(i, j int) {
		userSlice10000[i], userSlice10000[j] = userSlice10000[j], userSlice10000[i]
	})
	rand.Shuffle(len(userSlice100000), func(i, j int) {
		userSlice100000[i], userSlice100000[j] = userSlice100000[j], userSlice100000[i]
	})
	rand.Shuffle(len(userSlice1000000), func(i, j int) {
		userSlice1000000[i], userSlice1000000[j] = userSlice1000000[j], userSlice1000000[i]
	})
}

// generateSliceUsers генерирует X пользователей в срезе
func generateSliceUsers(x int) []User {
	IDCounter = 0
	users := make([]User, x)
	for i := 0; i <= x; i++ {
		users = append(users, generateRandomUser())
	}
	return users
}

// generateMapUsers генерирует X пользователей в карте
func generateMapUsers(x int) map[int]User {
	IDCounter = 0
	users := make(map[int]User, x)
	for i := 0; i <= x; i++ {
		users[i] = generateRandomUser()
	}
	return users
}

// Генерируем RandomUser
func generateRandomUser() User {
	//rand.Seed(time.Now().UnixNano())
	nameMax := len(Names)
	sirNameMax := len(SirNames)

	nameIndex := rand.Intn(nameMax-1) + 1
	sirNameIndex := rand.Intn(sirNameMax-1) + 1
	id := IDCounter
	IDCounter++
	return User{
		ID:   id,
		Name: Names[nameIndex] + " " + SirNames[sirNameIndex],
	}
}

// findUserInSlice ищет userID в срезе пользователей
func findUserInSlice(userID int, users []User) {
	// Теперь ищем предоставленный userID
	for _, user := range users {
		if user.ID == userID {
			return
		}
	}

}

// findUserInMap ищет конкретный ID в карте пользователей
func findUserInMap(userID int, users map[int]User) {
	if _, ok := users[userID]; ok {
		return
	}
}

// BenchmarkFindUserInSlice1000000 перебирает срез размером в 1000000 для поиска ID пользователя
func BenchmarkFindUserInSlice1000000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		findUserInSlice(7777, userSlice1000000)
	}
}

// BenchmarkFindUserInSlice100000 перебирает срез размером в 100000 для поиска ID пользователя
func BenchmarkFindUserInSlice100000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		findUserInSlice(7777, userSlice100000)
	}
}

// BenchmarkFindUserInSlice10000 перебирает срез размером в 10000 для поиска ID пользователя
func BenchmarkFindUserInSlice10000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		findUserInSlice(7777, userSlice10000)
	}
}

// BenchmarkFindUserInMap1000000 перебирает карту размером в 1000000 для поиска ID пользователя
func BenchmarkFindUserInMap1000000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		findUserInMap(7777, userMap1000000)
	}
}

// BenchmarkFindUserInMap100000 перебирает карту размером в 100000 для поиска ID пользователя
func BenchmarkFindUserInMap100000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		findUserInMap(7777, userMap100000)
	}
}

// BenchmarkFindUserInMap10000 перебирает карту размером в 10000 для поиска ID пользователя
func BenchmarkFindUserInMap10000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		findUserInMap(7777, userMap100000)
	}
}
