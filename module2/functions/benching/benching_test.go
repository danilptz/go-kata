package benching

import "testing"

func BenchmarkSimplest(b *testing.B) {
	for i := 0; i < b.N; i++ {
		x := i << 1
		_ = x
	}
}

func InsertXIntMap(x int, b *testing.B) {
	testMap := make(map[int]int, 0)
	b.ResetTimer()
	for i := 0; i < x; i++ {
		testMap[i] = i
	}
}

func BenchmarkInsertIntMap100000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		InsertXIntMap(100000, b)
	}
}

func BenchmarkInsertIntMap10000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		InsertXIntMap(10000, b)
	}
}

func BenchmarkInsertIntMap1000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		InsertXIntMap(1000, b)
	}
}

func BenchmarkName(b *testing.B) {
	for i := 0; i < b.N; i++ {
		InsertXIntMap(100, b)
	}
}
