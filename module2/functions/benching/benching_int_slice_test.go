package benching

import "testing"

func insertXIntSlice(x int, b *testing.B) {
	testSlice := make([]int, x)
	for i := 0; i < x; i++ {
		testSlice[i] = i
	}
	var holder int
	b.ResetTimer()
	for i := 0; i < x; i++ {
		holder = testSlice[i]
	}
	if holder != 0 {

	}
}

func BenchmarkInsertIntSlice100000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		insertXIntSlice(100000, b)
	}
}

func BenchmarkInsertIntSlice(b *testing.B) {
	for i := 0; i < b.N; i++ {
		insertXIntSlice(10000, b)
	}
}

func BenchmarkInsertIntSlice1000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		insertXIntSlice(1000, b)
	}
}

func BenchmarkInsertIntSlice100(b *testing.B) {
	for i := 0; i < b.N; i++ {
		insertXIntSlice(100, b)
	}
}
