package main

import (
	"errors"
	"fmt"
	"reflect"
)

type MergeDictsJob struct {
	Dicts      []map[string]string
	Merged     map[string]string
	IsFinished bool
}

var (
	errNotEnoughDicts = errors.New("at least 2 dictionaries are required")
	errNilDict        = errors.New("nil dictionary")
)

func main() {

	job := MergeDictsJob{}

	job1 := MergeDictsJob{
		Dicts: []map[string]string{
			{
				"a": "b",
			},
			nil,
		},
		Merged:     nil,
		IsFinished: false,
	}

	job2 := MergeDictsJob{
		Dicts: []map[string]string{
			{
				"a": "b",
			},
			{
				"b": "c",
			},
		},
		Merged:     nil,
		IsFinished: false,
	}
	jobs := []MergeDictsJob{job, job1, job2}

	for _, v := range jobs {
		if _, err := ExecuteMergeDictsJob(&v); err != nil {
			fmt.Printf("%v{IsFinished: %v, Dicts: %v{%v}}, \"%v\"\n", reflect.TypeOf(v), v.IsFinished, reflect.TypeOf(v.Dicts), v.Dicts, err)
		}
	}
}

func ExecuteMergeDictsJob(job *MergeDictsJob) (*MergeDictsJob, error) {

	defer func() { job.IsFinished = true }()

	job.Merged = make(map[string]string, len(job.Dicts))
	var err error

	if len(job.Dicts) < 2 {
		err = errNotEnoughDicts
	}

	if err == nil {
		for j := range job.Dicts {

			if job.Dicts[j] == nil {
				err = errNilDict
			}

			if err == nil {
				for k, v := range job.Dicts[j] {
					job.Merged[k] = v
				}
			}
		}
	}

	if err == nil {

		job.IsFinished = true
		fmt.Printf("%v{IsFinished: %v, Dicts: %v{{%v}}}, \"%v\"\n", reflect.TypeOf(*job), job.IsFinished, reflect.TypeOf(job.Dicts), job.Merged, err)
	}

	return job, err
}
