package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	t := time.Now()

	parseUrl("http://example.com")
	parseUrl("http://youtube.com")

	fmt.Printf("Parsing completed. Time Elapesed: %.2f seconds\n", time.Since(t).Seconds())
}

func parseUrl(url string) {
	for i := 0; i < 5; i++ {
		latency := rand.Intn(500) + 500

		time.Sleep(time.Duration(latency) * time.Millisecond)

		fmt.Printf("Parsing <%s> - Stop %d - Latency %d ms\n", url, i+1, latency)
	}
}
