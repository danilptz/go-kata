package main

import (
	"fmt"
	"time"
)

func main() {
	massage := make(chan string)

	go func() {
		for i := 1; i <= 10; i++ {
			massage <- fmt.Sprintf("%d", i)
			time.Sleep(time.Millisecond * 500)
		}

		close(massage)
	}()

	for {
		msg, open := <-massage
		if !open {
			break
		}

		fmt.Println(msg)
	}
}
