package main

import "fmt"

func main() {
	massege := make(chan string, 2)
	massege <- "hello"
	massege <- "world"

	fmt.Print(<-massege)
	fmt.Print(" ")
	massege <- "!"

	fmt.Print(<-massege)
	fmt.Print(<-massege)
}
