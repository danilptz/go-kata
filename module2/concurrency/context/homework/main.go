package main

import (
	"context"
	"fmt"
	"math/rand"
	"sync"
	"time"
)

func joinChannels(chs ...<-chan int) chan int {
	mergedCh := make(chan int)

	go func() {

		wg := &sync.WaitGroup{}
		wg.Add(len(chs))

		for _, ch := range chs {

			_, ok := <-ch
			if !ok {
				wg.Done()
				break
			}

			go func(ch <-chan int, wg *sync.WaitGroup) {

				defer wg.Done()

				for id := range ch {
					mergedCh <- id
				}

			}(ch, wg)

		}

		wg.Wait()
		close(mergedCh)

	}()

	return mergedCh
}

func generateData() chan int {
	out := make(chan int, 1000)

	go func() {

		defer close(out)

		for {

			select {
			case _, ok := <-out:
				if !ok {
					return
				}
			case out <- rand.Intn(100):
			}

		}

	}()

	return out
}

func main() {

	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	ticker := time.NewTicker(500 * time.Millisecond)
	done := make(chan bool)

	a := make(chan int)
	b := make(chan int)
	c := make(chan int)

	out := generateData()

	go func() {

		for num := range out {

			select {
			case <-done:
				return
			case <-ticker.C:
				a <- num
			}

		}

		close(a)
	}()

	go func() {

		for num := range out {

			select {
			case <-done:
				return
			case <-ticker.C:
				b <- num
			}

		}

		close(b)
	}()

	go func() {

		for num := range out {

			select {
			case <-done:
				return
			case <-ticker.C:
				c <- num
			}

		}

		close(c)
	}()

	mainChan := joinChannels(a, b, c)
	defer close(mainChan)

	go func() {

		for {

			select {
			case <-done:
				return
			case num := <-mainChan:
				fmt.Println(num)
			}

		}

	}()

	select {
	case <-ctx.Done():
		ticker.Stop()
		done <- true
	}

}
