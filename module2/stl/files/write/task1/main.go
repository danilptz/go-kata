package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	/*
		c := exec.Command("pwd")
		c.Stdin = os.Stdin
		c.Stdout = os.Stdout
		c.Stderr = os.Stderr
		c.Run()
	*/
	reader := bufio.NewReader(os.Stdin)

	fmt.Print("Enter your name: ")

	name, _ := reader.ReadString('\n')

	s := fmt.Sprintf("Hello %s\n", name)

	f, err := os.Create("stl/files/write/task1/hello.txt")
	if err != nil {
		fmt.Println(err)
	}
	defer f.Close()

	w, err1 := f.WriteString(s)
	if err1 != nil {
		fmt.Println(err1)
	} else {
		fmt.Printf("Hello %s\n", name)
	}
	f.Sync()
	_ = w
}
