package main

import (
	"fmt"
	"github.com/essentialkaos/translit/v2"
	"io"
	"os"
)

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func main() {
	f, err := os.Open("stl/files/write/task2/example.txt")
	check(err)
	defer f.Close()

	text, errRead := io.ReadAll(f)
	check(errRead)

	newFile, errNewFile := os.Create("stl/files/write/task2/example.processed.txt")
	check(errNewFile)
	defer newFile.Close()

	if _, errWrite := newFile.WriteString(translit.EncodeToBGN(string(text))); errWrite != nil {
		panic(errWrite)
	} else {
		fmt.Println("read - translate - write operations: ok")
	}
}
