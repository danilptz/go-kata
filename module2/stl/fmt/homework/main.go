package main

import (
	"fmt"
	"math/rand"
)

type Person struct {
	Name  string
	Age   int
	Money float64
}

func main() {
	p := Person{Name: "Andy", Age: 18, Money: rand.Float64()}

	// вывод значений структуры
	fmt.Println("simple struct:", p)

	// вывод названий полей и их значений
	fmt.Printf("detailed struct: %+v\n", p)

	// вывод названий полей и их значений в виде инициализации
	fmt.Printf("Golang struct: %#v\n", p)

	// вывод сформированной строки
	fmt.Println(generateSelfStory(p.Name, p.Age, p.Money))
}

func generateSelfStory(name string, age int, money float64) string {
	res := fmt.Sprintf("Hello! My name is %v. I`m %v years old. And i also have $%.2f in my wallet right now.", name, age, money)
	return res
}
