package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"
)

type Config struct {
	AppName    string `json:"app_name"`
	Production bool   `json:"production"`
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}

// const file = "stl/flags/homework/config.json"
//const file = "./config.json"

func main() {
	value := flag.String("conf", "", "path config")
	flag.Parse()

	file := *value
	conf := Config{"MyTestApp", true}
	toFile, err := os.Create(file)
	if err != nil {
		fmt.Println("No flag argument")
		os.Exit(1)
	}
	defer toFile.Close()

	jsonConf, err := json.Marshal(conf)
	check(err)

	errWrite := os.WriteFile(file, jsonConf, 0666)
	check(errWrite)

	newFiile, errOpen := os.ReadFile(file)
	check(errOpen)

	newConf := Config{}
	errUnmarsh := json.Unmarshal(newFiile, &newConf)
	check(errUnmarsh)

	fmt.Printf("AppName: %v\nProduction: %v", newConf.AppName, newConf.Production)
}

// Пример вывода конфига
// Production: true
// AppName: мое тестовое приложение
