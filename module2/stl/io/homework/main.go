package main

import (
	"bytes"
	"fmt"
	"io"
	"os"
)

func main() {
	data := []string{
		"there is 3pm, but im still alive to write this code snippet",
		"чистый код лучше, чем заумный код",
		"ваш код станет наследием будущих программистов",
		"задумайтесь об этом",
	}

	// здесь расположите буфер
	buf := bytes.Buffer{}

	// запишите данные в буфер
	for _, v := range data {
		buf.WriteString(v + ";")
	}

	// создайте файл
	file, err := os.Create("stl/io/homework/example.txt")
	if err != nil {
		panic(err)
	} else {
		fmt.Println("create file: ok")
	}

	// запишите данные в файл
	for {
		line, err := buf.ReadString(';')
		if err != nil {
			if err == io.EOF {
				break
			}
		}

		line = line[:len(line)-1]
		n, errWrite := file.WriteString(line + "\n")
		if err != nil {
			panic(errWrite)
		} else {
			fmt.Println("write line: ok byte: ", n)
		}
	}

	// прочтите данные в новый буфер

	text, errRead := io.ReadAll(file)
	if errRead != nil {
		panic(errRead)
	} else {
		fmt.Println("read file: ok")
	}

	newBuf := bytes.Buffer{}
	newBuf.Write(text)

	// выведите данные из буфера buffer.String()
	fmt.Println(newBuf.String())
	// у вас все получится!
}
